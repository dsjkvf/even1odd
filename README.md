# even | odd

## About

**Even-Or-Odd** is a small script that will write down either even or odd lines of a provided file named `file.ext` to a new file named either `file-even.txt` or `file-odd.txt`.

## Usage

  - to produce `file-odd.ext`:
    - `even1odd odd file.ext` or `even1odd o file.ext` 
  - to produce `file-even.ext`:
    - `even1odd even file.ext` or `even1odd e file.ext`
